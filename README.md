Lattice Boltzmann codes employing the open source OpenLB framework [(https://www.openlb.net/)](https://www.openlb.net/) v1.3-0 for simulating three-dimensional isothermal incompressible flows in lid-driven cavities (single-sided wall motion). This includes cases of both uniform and oscillatory lid motions. Sample postprocessing codes are provided, which have to be appropriately modified for your purpose. **Note** : Though the possibility of a two-sided motion (for three-dimensional case) is available in these codes, it has not been entirely tested and validated.

**Installation**

Refer the official OpenLB website [(https://www.openlb.net/tech-reports/)](https://www.openlb.net/tech-reports/) for more details about the installation of OpenLB package. 

![q_criterion_plots](./images/q_criterion.png)

If you happen to use these codes in your work, please cite the reference below.

**Reference**

1. Sthavishtha R. Bhopalam, D. Arumuga Perumal, and Ajay Kumar Yadav, Fluid flow in three-dimensional oscillating lid-driven cavities, _Proceedings of the 8th International and 47th National Conference on Fluid Mechanics and Fluid Power (FMFP)_, December 9-11, 2020, Indian Institute of Technology (IIT) Guwahati, Assam, India. [[Full text of the article]](https://app.box.com/s/jfwag5w7nvymhmqsntqzefpvnvhmafc6)