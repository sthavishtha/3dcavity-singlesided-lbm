### pvpython script to plot streamlines from velocity profile data (in Tecplot readable file)
### https://www.paraview.org/Wiki/ParaView_and_Batch
### run as : pvpython (or pvbatch) name_of_this_file.py "$input_directory_name" "$.pvd_file_name" ...
### ... "output_directory_name"

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# user-entered input filenames, directories
directoryIn = sys.argv[1]
datasetIn = sys.argv[2]
directoryOut = sys.argv[3]

# create a new 'PVD Reader'
cavity3dvtkpvd = PVDReader(FileName=[directoryIn + datasetIn + '.pvd'])

# get animation scene
animationScene1 = GetAnimationScene()

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [1005, 485]

# show data in view
cavity3dvtkpvdDisplay = Show(cavity3dvtkpvd, renderView1)
# trace defaults for the display properties.
cavity3dvtkpvdDisplay.Representation = 'Outline'
cavity3dvtkpvdDisplay.ColorArrayName = [None, '']
cavity3dvtkpvdDisplay.OSPRayScaleArray = 'physPressure'
cavity3dvtkpvdDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
cavity3dvtkpvdDisplay.SelectOrientationVectors = 'None'
cavity3dvtkpvdDisplay.ScaleFactor = 0.10200000000000001
cavity3dvtkpvdDisplay.SelectScaleArray = 'None'
cavity3dvtkpvdDisplay.GlyphType = 'Arrow'
cavity3dvtkpvdDisplay.GlyphTableIndexArray = 'None'
cavity3dvtkpvdDisplay.DataAxesGrid = 'GridAxesRepresentation'
cavity3dvtkpvdDisplay.PolarAxes = 'PolarAxesRepresentation'
# cavity3dvtkpvdDisplay.SelectInputVectors = ['POINTS', 'physVelocity']
# cavity3dvtkpvdDisplay.WriteLog = ''

# reset view to fit data
renderView1.ResetCamera()

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(cavity3dvtkpvdDisplay, ('FIELD', 'vtkBlockColors'))

# show color bar/color legend
cavity3dvtkpvdDisplay.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'vtkBlockColors'
vtkBlockColorsLUT = GetColorTransferFunction('vtkBlockColors')

# create a new 'Slice'
slice1 = Slice(Input=cavity3dvtkpvd)
slice1.SliceType = 'Plane'
slice1.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice1.SliceType.Origin = [0.5, 0.5, 0.5]

# Properties modified on slice1.SliceType
slice1.SliceType.Normal = [1.0, 0.0, 0.0]

# show data in view
slice1Display = Show(slice1, renderView1)
# trace defaults for the display properties.
slice1Display.Representation = 'Surface'
slice1Display.ColorArrayName = [None, '']
slice1Display.OSPRayScaleArray = 'physPressure'
slice1Display.OSPRayScaleFunction = 'PiecewiseFunction'
slice1Display.SelectOrientationVectors = 'None'
slice1Display.ScaleFactor = 0.10199999902397394
slice1Display.SelectScaleArray = 'None'
slice1Display.GlyphType = 'Arrow'
slice1Display.GlyphTableIndexArray = 'None'
slice1Display.DataAxesGrid = 'GridAxesRepresentation'
slice1Display.PolarAxes = 'PolarAxesRepresentation'
# slice1Display.SelectInputVectors = ['POINTS', 'physVelocity']
# slice1Display.WriteLog = ''

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(slice1Display, ('FIELD', 'vtkBlockColors'))

# show color bar/color legend
slice1Display.SetScalarBarVisibility(renderView1, True)

# create a new 'Merge Blocks'
mergeBlocks1 = MergeBlocks(Input=slice1)

# show data in view
mergeBlocks1Display = Show(mergeBlocks1, renderView1)
# trace defaults for the display properties.
mergeBlocks1Display.Representation = 'Surface'
mergeBlocks1Display.ColorArrayName = [None, '']
mergeBlocks1Display.OSPRayScaleArray = 'physPressure'
mergeBlocks1Display.OSPRayScaleFunction = 'PiecewiseFunction'
mergeBlocks1Display.SelectOrientationVectors = 'None'
mergeBlocks1Display.ScaleFactor = 0.10199999902397394
mergeBlocks1Display.SelectScaleArray = 'None'
mergeBlocks1Display.GlyphType = 'Arrow'
mergeBlocks1Display.GlyphTableIndexArray = 'None'
mergeBlocks1Display.DataAxesGrid = 'GridAxesRepresentation'
mergeBlocks1Display.PolarAxes = 'PolarAxesRepresentation'
mergeBlocks1Display.ScalarOpacityUnitDistance = 0.05144134329263765
# mergeBlocks1Display.SelectInputVectors = ['POINTS', 'physVelocity']
# mergeBlocks1Display.WriteLog = ''

# hide data in view
Hide(slice1, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(cavity3dvtkpvd)

# create a new 'Slice'
slice2 = Slice(Input=cavity3dvtkpvd)
slice2.SliceType = 'Plane'
slice2.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice2.SliceType.Origin = [0.5, 0.5, 0.5]

# Properties modified on slice2.SliceType
slice2.SliceType.Normal = [0.0, 1.0, 0.0]

# show data in view
slice2Display = Show(slice2, renderView1)
# trace defaults for the display properties.
slice2Display.Representation = 'Surface'
slice2Display.ColorArrayName = [None, '']
slice2Display.OSPRayScaleArray = 'physPressure'
slice2Display.OSPRayScaleFunction = 'PiecewiseFunction'
slice2Display.SelectOrientationVectors = 'None'
slice2Display.ScaleFactor = 0.10199999902397394
slice2Display.SelectScaleArray = 'None'
slice2Display.GlyphType = 'Arrow'
slice2Display.GlyphTableIndexArray = 'None'
slice2Display.DataAxesGrid = 'GridAxesRepresentation'
slice2Display.PolarAxes = 'PolarAxesRepresentation'
# slice2Display.SelectInputVectors = ['POINTS', 'physVelocity']
# slice2Display.WriteLog = ''

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(slice2Display, ('FIELD', 'vtkBlockColors'))

# show color bar/color legend
slice2Display.SetScalarBarVisibility(renderView1, True)

# create a new 'Merge Blocks'
mergeBlocks2 = MergeBlocks(Input=slice2)

# show data in view
mergeBlocks2Display = Show(mergeBlocks2, renderView1)
# trace defaults for the display properties.
mergeBlocks2Display.Representation = 'Surface'
mergeBlocks2Display.ColorArrayName = [None, '']
mergeBlocks2Display.OSPRayScaleArray = 'physPressure'
mergeBlocks2Display.OSPRayScaleFunction = 'PiecewiseFunction'
mergeBlocks2Display.SelectOrientationVectors = 'None'
mergeBlocks2Display.ScaleFactor = 0.10199999902397394
mergeBlocks2Display.SelectScaleArray = 'None'
mergeBlocks2Display.GlyphType = 'Arrow'
mergeBlocks2Display.GlyphTableIndexArray = 'None'
mergeBlocks2Display.DataAxesGrid = 'GridAxesRepresentation'
mergeBlocks2Display.PolarAxes = 'PolarAxesRepresentation'
mergeBlocks2Display.ScalarOpacityUnitDistance = 0.05160569374883775
# mergeBlocks2Display.SelectInputVectors = ['POINTS', 'physVelocity']
# mergeBlocks2Display.WriteLog = ''

# hide data in view
Hide(slice2, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(cavity3dvtkpvd)

# create a new 'Slice'
slice3 = Slice(Input=cavity3dvtkpvd)
slice3.SliceType = 'Plane'
slice3.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice3.SliceType.Origin = [0.5, 0.5, 0.5]

# Properties modified on slice3.SliceType
slice3.SliceType.Normal = [0.0, 0.0, 1.0]

# show data in view
slice3Display = Show(slice3, renderView1)
# trace defaults for the display properties.
slice3Display.Representation = 'Surface'
slice3Display.ColorArrayName = [None, '']
slice3Display.OSPRayScaleArray = 'physPressure'
slice3Display.OSPRayScaleFunction = 'PiecewiseFunction'
slice3Display.SelectOrientationVectors = 'None'
slice3Display.ScaleFactor = 0.10199999902397394
slice3Display.SelectScaleArray = 'None'
slice3Display.GlyphType = 'Arrow'
slice3Display.GlyphTableIndexArray = 'None'
slice3Display.DataAxesGrid = 'GridAxesRepresentation'
slice3Display.PolarAxes = 'PolarAxesRepresentation'
# slice3Display.SelectInputVectors = ['POINTS', 'physVelocity']
# slice3Display.WriteLog = ''

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(slice3Display, ('FIELD', 'vtkBlockColors'))

# show color bar/color legend
slice3Display.SetScalarBarVisibility(renderView1, True)

# create a new 'Merge Blocks'
mergeBlocks3 = MergeBlocks(Input=slice3)

# show data in view
mergeBlocks3Display = Show(mergeBlocks3, renderView1)
# trace defaults for the display properties.
mergeBlocks3Display.Representation = 'Surface'
mergeBlocks3Display.ColorArrayName = [None, '']
mergeBlocks3Display.OSPRayScaleArray = 'physPressure'
mergeBlocks3Display.OSPRayScaleFunction = 'PiecewiseFunction'
mergeBlocks3Display.SelectOrientationVectors = 'None'
mergeBlocks3Display.ScaleFactor = 0.10199999902397394
mergeBlocks3Display.SelectScaleArray = 'None'
mergeBlocks3Display.GlyphType = 'Arrow'
mergeBlocks3Display.GlyphTableIndexArray = 'None'
mergeBlocks3Display.DataAxesGrid = 'GridAxesRepresentation'
mergeBlocks3Display.PolarAxes = 'PolarAxesRepresentation'
mergeBlocks3Display.ScalarOpacityUnitDistance = 0.05160569374883775
# mergeBlocks3Display.SelectInputVectors = ['POINTS', 'physVelocity']
# mergeBlocks3Display.WriteLog = ''

# hide data in view
Hide(slice3, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(mergeBlocks1)

# save data
SaveData(directoryOut + 'yz.vtk', proxy=mergeBlocks1, Writealltimestepsasfileseries=1)

# set active source
SetActiveSource(mergeBlocks2)

# save data
SaveData(directoryOut + 'xz.vtk', proxy=mergeBlocks2, Writealltimestepsasfileseries=1)

# set active source
SetActiveSource(mergeBlocks3)

# save data
SaveData(directoryOut + 'xy.vtk', proxy=mergeBlocks3, Writealltimestepsasfileseries=1)

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.CameraPosition = [0.4999999953433871, 0.4999999953433871, 3.912986481935175]
renderView1.CameraFocalPoint = [0.4999999953433871, 0.4999999953433871, 0.4999999953433871]
renderView1.CameraParallelScale = 0.8833459034074937

#### uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).