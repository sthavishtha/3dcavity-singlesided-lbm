### pvpython script to plot streamlines from velocity profile data (in Tecplot readable file)
### https://www.paraview.org/Wiki/ParaView_and_Batch
### run as : pvpython (or pvbatch) name_of_this_file.py "$input_directory_name" "$tecplot_file_name" ...
### ... "output_directory_name"

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# user-entered input filenames, directories
directoryIn = sys.argv[1]
datasetIn = sys.argv[2]
directoryOut = sys.argv[3]
imageFileName = sys.argv[4]
plane = sys.argv[5]

# create a new 'Legacy VTK Reader'
vtk_reader = LegacyVTKReader(FileNames=[directoryIn + datasetIn + '.vtk']) 
# FileNames=['/media/sbhopala/9A76296B76294979/sthavishtha/publications/journals/nasa_national/codes/openlb_codes/tmp/extensions/tmp/paper/re100_2pi6_v3/tmp/imageData/data/xy_025.vtk'])

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [1005, 485]

# show data in view
vtk_readerDisplay = Show(vtk_reader, renderView1)
# trace defaults for the display properties.
vtk_readerDisplay.Representation = 'Surface'
vtk_readerDisplay.ColorArrayName = [None, '']
vtk_readerDisplay.OSPRayScaleArray = 'physPressure'
vtk_readerDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
vtk_readerDisplay.SelectOrientationVectors = 'None'
vtk_readerDisplay.ScaleFactor = 0.10199999902397394
vtk_readerDisplay.SelectScaleArray = 'None'
vtk_readerDisplay.GlyphType = 'Arrow'
vtk_readerDisplay.GlyphTableIndexArray = 'None'
vtk_readerDisplay.DataAxesGrid = 'GridAxesRepresentation'
vtk_readerDisplay.PolarAxes = 'PolarAxesRepresentation'
vtk_readerDisplay.ScalarOpacityUnitDistance = 0.05160569374883775
# vtk_readerDisplay.SelectInputVectors = ['POINTS', 'physVelocity']
# vtk_readerDisplay.WriteLog = ''

# reset view to fit data
renderView1.ResetCamera()

#changing interaction mode based on data extents
renderView1.InteractionMode = '2D'
if (plane == 'xy') :
	renderView1.CameraPosition = [0.5, 0.5, 10000.5]
elif (plane == 'yz') :
	renderView1.CameraPosition = [10000.5, 0.5, 0.5]
elif (plane == 'xz') :
	renderView1.CameraPosition = [0.5, 10000.5, 0.5]
	renderView1.CameraViewUp = [1.0, 0.0, 0.0]

renderView1.CameraFocalPoint = [0.5, 0.5, 0.5]

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Calculator'
calculator1 = Calculator(Input=vtk_reader)
calculator1.Function = ''

if ((plane == 'yz') | (plane == 'xz')) :

	# Properties modified on calculator1
	calculator1.ResultArrayName = 'vel'

	if (plane == 'yz') :
		calculator1.Function = 'physVelocity_Y*jHat+physVelocity_Z*kHat'
	else :
		calculator1.Function = 'physVelocity_X*iHat+physVelocity_Z*kHat'

	# show data in view
	calculator1Display = Show(calculator1, renderView1)
	# trace defaults for the display properties.
	calculator1Display.Representation = 'Surface'
	calculator1Display.ColorArrayName = [None, '']
	calculator1Display.OSPRayScaleArray = 'physPressure'
	calculator1Display.OSPRayScaleFunction = 'PiecewiseFunction'
	calculator1Display.SelectOrientationVectors = 'vel'
	calculator1Display.ScaleFactor = 0.10199999902397394
	calculator1Display.SelectScaleArray = 'None'
	calculator1Display.GlyphType = 'Arrow'
	calculator1Display.GlyphTableIndexArray = 'None'
	calculator1Display.DataAxesGrid = 'GridAxesRepresentation'
	calculator1Display.PolarAxes = 'PolarAxesRepresentation'
	calculator1Display.ScalarOpacityUnitDistance = 0.05160569374883775
	# calculator1Display.SelectInputVectors = ['POINTS', 'vel']
	# calculator1Display.WriteLog = ''

	# hide data in view
	Hide(vtk_reader, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Mask Points'
if (plane == 'xy') :
	maskPoints1 = MaskPoints(Input=vtk_reader)
elif ((plane == 'yz') | (plane == 'xz')) :
	maskPoints1 = MaskPoints(Input=calculator1)

# Properties modified on maskPoints1
maskPoints1.MaximumNumberofPoints = 100
maskPoints1.ProportionallyDistributeMaximumNumberOfPoints = 1
maskPoints1.RandomSampling = 1

# show data in view
maskPoints1Display = Show(maskPoints1, renderView1)
# trace defaults for the display properties.
maskPoints1Display.Representation = 'Surface'
maskPoints1Display.ColorArrayName = [None, '']
maskPoints1Display.OSPRayScaleArray = 'physPressure'
maskPoints1Display.OSPRayScaleFunction = 'PiecewiseFunction'
if (plane == 'xy') :
	maskPoints1Display.SelectOrientationVectors = 'None'
elif ((plane == 'yz') | (plane == 'xz')) :
	maskPoints1Display.SelectOrientationVectors = 'vel'
maskPoints1Display.ScaleFactor = 0.10099999997764826
maskPoints1Display.SelectScaleArray = 'None'
maskPoints1Display.GlyphType = 'Arrow'
maskPoints1Display.GlyphTableIndexArray = 'None'
maskPoints1Display.DataAxesGrid = 'GridAxesRepresentation'
maskPoints1Display.PolarAxes = 'PolarAxesRepresentation'
# maskPoints1Display.SelectInputVectors = ['POINTS', 'physVelocity']
# maskPoints1Display.WriteLog = ''

# hide data in view
if (plane == 'xy') :
	Hide(vtk_reader, renderView1)
elif ((plane == 'yz') | (plane == 'xz')) :
	Hide(calculator1, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Stream Tracer With Custom Source'
if (plane == 'xy') :
	streamTracerWithCustomSource1 = StreamTracerWithCustomSource(Input=vtk_reader,
    	SeedSource=maskPoints1)
	streamTracerWithCustomSource1.Vectors = ['POINTS', 'physVelocity']
elif ((plane == 'yz') | (plane == 'xz')) :
	streamTracerWithCustomSource1 = StreamTracerWithCustomSource(Input=calculator1,
    	SeedSource=maskPoints1)	
	streamTracerWithCustomSource1.Vectors = ['POINTS', 'vel']
streamTracerWithCustomSource1.MaximumStreamlineLength = 1.0199999902397394

# show data in view
streamTracerWithCustomSource1Display = Show(streamTracerWithCustomSource1, renderView1)
# trace defaults for the display properties.
streamTracerWithCustomSource1Display.Representation = 'Surface'
streamTracerWithCustomSource1Display.ColorArrayName = [None, '']
streamTracerWithCustomSource1Display.OSPRayScaleArray = 'AngularVelocity'
streamTracerWithCustomSource1Display.OSPRayScaleFunction = 'PiecewiseFunction'
streamTracerWithCustomSource1Display.SelectOrientationVectors = 'Normals'
streamTracerWithCustomSource1Display.ScaleFactor = 0.10039563400205226
streamTracerWithCustomSource1Display.SelectScaleArray = 'AngularVelocity'
streamTracerWithCustomSource1Display.GlyphType = 'Arrow'
streamTracerWithCustomSource1Display.GlyphTableIndexArray = 'AngularVelocity'
streamTracerWithCustomSource1Display.DataAxesGrid = 'GridAxesRepresentation'
streamTracerWithCustomSource1Display.PolarAxes = 'PolarAxesRepresentation'
# streamTracerWithCustomSource1Display.SelectInputVectors = ['POINTS', 'Normals']
# streamTracerWithCustomSource1Display.WriteLog = ''

# hide data in view
if (plane == 'xy') :
	Hide(vtk_reader, renderView1)
elif ((plane == 'yz') | (plane == 'xz')) :
	Hide(calculator1, renderView1)

# hide data in view
Hide(maskPoints1, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on renderView1.AxesGrid
renderView1.AxesGrid.Visibility = 1

# current camera placement for renderView1
renderView1.InteractionMode = '2D'

if (plane == 'xy') :
	renderView1.CameraPosition = [0.5, 0.5, 10000.5]
elif (plane == 'yz') :
	renderView1.CameraPosition = [10000.5, 0.5, 0.5]
elif (plane == 'xz') :
	renderView1.CameraPosition = [0.5, 10000.5, 0.5]
	renderView1.CameraViewUp = [1.0, 0.0, 0.0]

renderView1.CameraFocalPoint = [0.5, 0.5, 0.5]
renderView1.CameraParallelScale = 0.721248909908732

# save screenshot
SaveScreenshot(directoryOut + imageFileName + '.png', renderView1, ImageResolution=[1005, 485])

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.InteractionMode = '2D'

if (plane == 'xy') :
	renderView1.CameraPosition = [0.5, 0.5, 10000.5]
elif (plane == 'yz') :
	renderView1.CameraPosition = [10000.5, 0.5, 0.5]
elif (plane == 'xz') :
	renderView1.CameraPosition = [0.5, 10000.5, 0.5]
	renderView1.CameraViewUp = [1.0, 0.0, 0.0]

renderView1.CameraFocalPoint = [0.5, 0.5, 0.5]
renderView1.CameraParallelScale = 0.721248909908732

#### uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).