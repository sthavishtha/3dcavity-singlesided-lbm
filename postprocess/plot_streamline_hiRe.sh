### bash script to plot sliced streamlines along x,y,z directions from .pvd 3D files 
### Example : $ bash plot_streamline.sh re100_2pi6_v3
#!/bin/bash

dir_name=$1
dir_imgname="$dir_name/tmp/imageData/data"
dir_vtkname="$dir_name/tmp/vtkData"

time=("25" "50" "75" "100")

# obtain the sliced data from paraview into .vtk files
# echo pvpython save_slices.py "$dir_vtkname/" "cavity3dvtk.pvd" "$dir_imgname/"
pvpython save_slices.py "$dir_vtkname/" "cavity3dvtk" "$dir_imgname/" 
echo "sliced data into .vtk files successfully"

# rename the sliced .vtk files
for i in {0..3}
do
	# echo "$dir_imgname/xy_${i}.vtk" "$dir_imgname/xy_${time[i]}.vtk" 
	mv "$dir_imgname/xy_${i}.vtk" "$dir_imgname/xy_${time[i]}.vtk"
	mv "$dir_imgname/yz_${i}.vtk" "$dir_imgname/yz_${time[i]}.vtk"
	mv "$dir_imgname/xz_${i}.vtk" "$dir_imgname/xz_${time[i]}.vtk"
done
echo "renamed .vtk files successfully"

# plot streamlines from the sliced .vtk files
for i in "${time[@]}"
do
	# echo pvpython streamlines.py "$dir_imgname/" "xy_${i}" "$dir_imgname/" "xy_${i}"
	pvpython streamlines.py "$dir_imgname/" "xy_${i}" "$dir_imgname/" "xy_${i}" "xy"
	pvpython streamlines.py "$dir_imgname/" "yz_${i}" "$dir_imgname/" "yz_${i}" "yz"
	pvpython streamlines.py "$dir_imgname/" "xz_${i}" "$dir_imgname/" "xz_${i}" "xz"
done
echo "surface streamlines plotted successfully"